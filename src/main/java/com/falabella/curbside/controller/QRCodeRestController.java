package com.falabella.curbside.controller;

import java.io.IOException;
import java.util.Base64;

import com.falabella.curbside.generator.QRCodeGenerator;
import com.google.zxing.WriterException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class QRCodeRestController {

    @GetMapping("/generate_code/{unique-id}")
    public String getQRCode(@PathVariable("unique-id") String key){
        byte[] image = new byte[0];
        try {

            // Generate and Return Qr Code in Byte Array
            image = QRCodeGenerator.getQRCodeImage(key,250,250);

        } catch (WriterException | IOException e) {
            e.printStackTrace();
        }
        // Convert Byte Array into Base64 Encode String
        String qrcode = Base64.getEncoder().encodeToString(image);

        return qrcode;
    }
}
